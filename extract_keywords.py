import json
import os

import click
import openai
import requests
from bs4 import BeautifulSoup


# Config class that reads configuration files from
# local directory or the users home directory
# configuration file is JSON format
class Config:
    def __init__(self, config_file):
        self.config_file = config_file

    def get_config(self):
        try:
            with open(self.config_file, "r") as f:
                return json.load(f)
        except FileNotFoundError:
            home_dir = os.path.expanduser("~")
            home_config_file = os.path.join(home_dir, self.config_file)
            with open(home_config_file, "r") as f:
                return json.load(f)


# Refactor the code below into a class called ExtractKeywords
# The class should have the following methods:
# - extract_text_from_html
# - retrieve_html
# - get_keywords_from_openai
# Initialize the class with a config object
class ExtractKeywords:
    def __init__(self, config, url, save_text=False):
        self.config = config
        self.url = url
        self.save_text = save_text

    def retrieve_html(self):
        response = requests.get(self.url)
        return response.text

    def extract_text_from_html(self, html):
        soup = BeautifulSoup(html, "html.parser")
        # if the url is a linkedin url, get the text from class decorated-job-posting__details
        if "linkedin" in self.url:
            soup = soup.find("div", class_="decorated-job-posting__details")
        elif "greenhouse" in self.url:
            soup = soup.find("div", id="content")
        elif "bebee" in self.url:
            soup = soup.find("div", class_="description-job-tr")
        else:
            # if the url is not a linkedin url, get the text from the body tag
            soup = soup.find("body")

        # return the text from the soup object
        return soup.text

    def get_keywords_from_openai(self, text):
        prompt = "You are a software product manager. From the job description below provide a comma separated list of keywords that are relevant to software product managers."
        prompt += "Job Description:\n"
        prompt += text
        prompt += "\nKeywords:\n"

        openai.api_key = self.config["openai_api_key"]
        response = openai.Completion.create(
            engine="text-davinci-003",
            prompt=prompt,
            max_tokens=500,
            temperature=0,
            top_p=1,
            frequency_penalty=0,
            presence_penalty=0,
        )
        return response.choices[0].text

    def convert_to_list(self, keywords):
        # remove leading and trailing whitespace
        keyword_list = keywords.split(",")
        keyword_list = [keyword.strip() for keyword in keyword_list]
        return keyword_list

    def generate_filename(self):
        # generate a filename based on the url
        # remove the https:// from the url
        filename = self.url.replace("https://", "")
        # replace all / with _
        filename = filename.replace("/", "_")
        # add .txt to the end of the filename
        filename += ".txt"
        return filename

    def write_to_file(self, result):
        output = self.generate_filename()
        with open(output, "a") as f:
            for keyword in result:
                f.write(f"{keyword}\n")

    def extract(self):
        html = self.retrieve_html()
        text = self.extract_text_from_html(html)
        # strip out newlines from text
        text = text.replace("\n", " ")
        if self.save_text:
            filename = self.generate_filename()
            with open(f"job-{filename}", "w") as f:
                f.write(text)
        keywords = self.get_keywords_from_openai(text)
        return self.convert_to_list(keywords)


# command line interface
# --url is a required argument
# --output is an optional flag
@click.command()
@click.option("--url", required=True, prompt=True, help="URL to extract keywords from")
@click.option("--output", is_flag=True, help="Output file to write keywords to")
@click.option("--save-text", is_flag=True, help="Save job description text to file")
def main(url, output, save_text):
    config = Config("config.json").get_config()
    extract_keywords = ExtractKeywords(config, url, save_text)
    result = extract_keywords.extract()
    if output:
        extract_keywords.write_to_file(result)
    else:
        for keyword in result:
            print(keyword)


if __name__ == "__main__":
    main()
