# Load a collection of text files from a directory and count
# how many times each word appears in the files.

import os

import click
import nltk
import nltk.data


class WordCounter:
    def __init__(self, directory):
        self.directory = directory
        self.filenames = []

        # get the list of filenames in the directory
        for filename in os.listdir(self.directory):
            if filename.endswith(".txt"):
                self.filenames.append(filename)

    # Count the number of times each phrase appears in the text files
    # each line of the file is a unique phrase.
    def count_phrases(self):
        phrase_counts = {}
        for filename in self.filenames:
            with open(os.path.join(self.directory, filename), "r") as f:
                for line in f:
                    line = line.strip()
                    if line in phrase_counts:
                        phrase_counts[line] += 1
                    else:
                        phrase_counts[line] = 1
        return phrase_counts

    # def count_phrases(self):
    #     phrase_counts = {}
    #     for filename in self.filenames:
    #         with open(os.path.join(self.directory, filename), "r") as f:
    #             text = f.read()
    #             tokenizer = nltk.data.load("tokenizers/punkt/english.pickle")
    #             sentences = tokenizer.tokenize(text)
    #             for sentence in sentences:
    #                 tokens = nltk.word_tokenize(sentence)
    #                 for i in range(len(tokens) - 1):
    #                     phrase = f"{tokens[i]} {tokens[i + 1]}"
    #                     if phrase in phrase_counts:
    #                         phrase_counts[phrase] += 1
    #                     else:
    #                         phrase_counts[phrase] = 1
    #     return phrase_counts

    # def count_words(self):
    #     word_counts = {}
    #     for filename in self.filenames:
    #         with open(os.path.join(self.directory, filename), "r") as f:
    #             text = f.read()
    #             tokens = nltk.word_tokenize(text)
    #             for token in tokens:
    #                 if token in word_counts:
    #                     word_counts[token] += 1
    #                 else:
    #                     word_counts[token] = 1
    #     return word_counts


# create a clik command line interface to load text files from a directory
# with the parameters --directory
# and print the word counts to the console
@click.command()
@click.option("--directory", required=True, prompt=True, help="Directory to load text files from")
def main(directory):
    word_counter = WordCounter(directory)
    word_counts = word_counter.count_phrases()
    # sort the word counts by the number of times the word appears
    word_counts = dict(sorted(word_counts.items(), key=lambda item: item[1], reverse=True))
    for word, count in word_counts.items():
        print(f"{word}: {count}")


if __name__ == "__main__":
    main()
