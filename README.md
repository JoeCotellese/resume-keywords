# Extract Keywords

Extract the keywords from job descriptions so you can make sure they are discussed in your resume.

## Installation

### Dev

I use python and [pipenv](https://docs.pipenv.org/) as a primary tools for
development. See [Pipfile](Pipfile), [Pipfile.lock](Pipfile.lock), for full specification of
platform, python and dependency packages.

Basically, to reproduce enviroment, you need to run `pipenv install`.

## Usage

There are two scripts - extract_keywords.py and freq.py

You can run --help on both to see the parameters.

## License

[MIT](LICENSE)
